package com.training.hospital;

import com.training.hospital.status.HealthStatus;
import com.training.hospital.utils.DefaultParser;
import com.training.hospital.utils.Parser;
import com.training.hospital.medecine.Medecine;
import com.training.hospital.patient.Patient;
import com.training.hospital.utils.DefaultPrinter;
import com.training.hospital.utils.Printer;

import java.util.List;
import java.util.Map;

/**
 * Created by user on 09/05/2016.
 */
public class Quarantine {
    private final List<Patient> patients;
    private final Printer printer;
    private final Parser parser;

    public Quarantine(String patients) {
        printer = new DefaultPrinter();
        parser = new DefaultParser();
        this.patients = parser.createPatients(patients);
    }

    public String report() {
        Map<HealthStatus, Integer> statistics = lauchStatistics();
        return printer.print(statistics);

    }

    private Map<HealthStatus, Integer> lauchStatistics() {
        Map<HealthStatus, Integer> statistics = printer.initializeStatistics();
        for (Patient currentPatient : patients) {
            currentPatient.beCounted(statistics);
        }
        return statistics;
    }

    public void wait40Days() {
        for (Patient currentPatient : patients) {
            currentPatient.applyTreatment();
        }
    }

    public void aspirin() {
        for (Patient currentPatient : patients) {
            currentPatient.addMedecine(Medecine.ASPIRIN);
        }
    }

    public void antibiotic() {
        for (Patient currentPatient : patients) {
            currentPatient.addMedecine(Medecine.ANTIBIOTIC);
        }

    }

    public void insulin() {
        for (Patient currentPatient : patients) {
            currentPatient.addMedecine(Medecine.INSULIN);
        }
    }

    public void paracetamol() {
        for (Patient currentPatient : patients) {
            currentPatient.addMedecine(Medecine.PARACETAMOL);
        }
    }
}
