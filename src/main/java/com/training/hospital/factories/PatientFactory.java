package com.training.hospital.factories;

import com.training.hospital.patient.*;
import com.training.hospital.status.HealthStatus;

/**
 * Created by user on 09/05/2016.
 */
public class PatientFactory {

    public static Patient createPatient(String currentPatient) {
        HealthStatus status = HealthStatus.valueOf(currentPatient);
         return new Patient(status) ;
    }
}
