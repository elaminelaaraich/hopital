package com.training.hospital.medecine;

/**
 * Created by user on 09/05/2016.
 */
public enum Medecine {
    INSULIN, ANTIBIOTIC, PARACETAMOL, ASPIRIN
}
