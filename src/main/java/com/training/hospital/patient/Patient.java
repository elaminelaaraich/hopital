package com.training.hospital.patient;

import com.training.hospital.status.HealthStatus;
import com.training.hospital.medecine.Medecine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 09/05/2016.
 */
public class Patient {

    public static final int ONE_OCCURENCE = 1;
    private HealthStatus healthStatus;
    private final List<Medecine> medecines;


    public Patient(HealthStatus healthStatus) {

        this.healthStatus = healthStatus;
        medecines = new ArrayList<>();
    }

    public void beCounted(Map<HealthStatus, Integer> statistics) {
        statistics.put(healthStatus, statistics.get(healthStatus) + ONE_OCCURENCE);
    }

    public void applyTreatment() {
        this.healthStatus = this.healthStatus.cure(medecines);
    }

    public void addMedecine(Medecine medecine) {
        medecines.add(medecine);
        this.healthStatus = treatment();
    }

    private HealthStatus treatment() {
        boolean willKill = medecines.containsAll(Arrays.asList(Medecine.ASPIRIN, Medecine.PARACETAMOL));
        if (willKill) {
            return HealthStatus.X;
        }
        return healthStatus;
    }


}
