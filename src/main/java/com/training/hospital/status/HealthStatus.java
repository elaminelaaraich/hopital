package com.training.hospital.status;

import com.training.hospital.medecine.Medecine;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 09/05/2016.
 */
public enum HealthStatus {
    F("Fever") {
        @Override
        public HealthStatus cure(List<Medecine> medecines) {
            boolean willBeCured = medecines.contains(Medecine.ASPIRIN) || medecines.contains(Medecine.PARACETAMOL);
            return willBeCured ? H : this;
        }
    }, H("Healthy") {
        @Override
        public HealthStatus cure(List<Medecine> medecines) {
            boolean willBeCured = medecines.contains(Medecine.INSULIN) && medecines.contains(Medecine.ANTIBIOTIC);
            return willBeCured ? F : this;
        }
    }, D("Diabetes") {
        @Override
        public HealthStatus cure(List<Medecine> medecines) {
            boolean willBeCured = medecines.contains(Medecine.INSULIN);
            return willBeCured ? this : X;
        }
    }, T("Tuberculosis") {
        @Override
        public HealthStatus cure(List<Medecine> medecines) {
            boolean willBeCured = medecines.contains(Medecine.ANTIBIOTIC);
            return willBeCured ? H : this;
        }
    }, X("Dead") {
        @Override
        public HealthStatus cure(List<Medecine> medecines) {
            return this;
        }
    };

    private String name;

    HealthStatus(String name) {
        this.name = name;
    }


    public abstract HealthStatus cure(List<Medecine> medecines);
}
