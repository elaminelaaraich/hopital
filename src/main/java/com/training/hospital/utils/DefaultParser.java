package com.training.hospital.utils;

import com.training.hospital.factories.PatientFactory;
import com.training.hospital.patient.Patient;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 09/05/2016.
 */
public class DefaultParser implements Parser {

    public static final String SEPARATOR = ",";


    @Override
    public List<Patient> createPatients(String patientsInput) {
        String[] patients = patientsInput.split(SEPARATOR);
        List<Patient> patientsList = new ArrayList<>();
        for (String currentPatient : patients) {
            Patient patient = PatientFactory.createPatient(currentPatient);
            patientsList.add(patient);
        }
        return patientsList;
    }
}
