package com.training.hospital.utils;

import com.sun.deploy.util.StringUtils;
import com.training.hospital.status.HealthStatus;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 09/05/2016.
 */
public class DefaultPrinter implements Printer {
    private static final String SEPARATOR = " ";
    public static final String INTER_SEPARATOR = ":";
    public static final int INITIAL_VALUE = 0;

    public String print(Map<HealthStatus, Integer> statistics) {
        List<String> toReport = new ArrayList<String>();
        for (HealthStatus currentStatus : statistics.keySet()) {
            Integer numberOfOccurence = statistics.get(currentStatus);
            toReport.add(printHealth(currentStatus, numberOfOccurence));
        }
        return StringUtils.join(toReport, SEPARATOR);
    }

    private String printHealth(HealthStatus currentStatus, Integer numberOfOccurence) {
        return currentStatus.toString() + INTER_SEPARATOR + numberOfOccurence;
    }

    public  Map<HealthStatus, Integer> initializeStatistics() {
        Map<HealthStatus, Integer> statistics = new LinkedHashMap<>();
        for (HealthStatus currentStatus : HealthStatus.values()) {
            statistics.put(currentStatus, INITIAL_VALUE);
        }
        return statistics;
    }
}
