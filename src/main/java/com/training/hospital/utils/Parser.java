package com.training.hospital.utils;

import com.training.hospital.patient.Patient;

import java.util.List;

/**
 * Created by user on 09/05/2016.
 */
public interface Parser {
    List<Patient> createPatients(String patients);
}
