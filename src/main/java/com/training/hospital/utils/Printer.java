package com.training.hospital.utils;

import com.training.hospital.status.HealthStatus;

import java.util.Map;

/**
 * Created by user on 09/05/2016.
 */
public interface Printer {
    String print(Map<HealthStatus, Integer> statistics);

    Map<HealthStatus,Integer> initializeStatistics();
}
