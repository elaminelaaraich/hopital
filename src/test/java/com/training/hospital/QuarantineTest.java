package com.training.hospital;

import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class QuarantineTest {

    /**
     * The responsibility of the Quarantine object is to simulate diseases on a
     * group of patients. It is initialized with a list of patients' health
     * status, separated by a comma. Each health status is described by one or
     * more characters (in the test below, we will always have only one disease
     * / patient)
     * <p>
     * The characters mean: H : Healthy F : Fever D : Diabetes T : Tuberculosis
     * <p>
     * Quarantine provides medicines to the patients, but can not target a
     * specific group of patient. The same medicines are always given to all the
     * patients.
     * <p>
     * Then Quarantine can provide a report with this format:
     * "F:1 H:2 D:0 T:1 X:3"
     * <p>
     * Report give the number of patients that have the given disease. X means
     * Dead
     */

    @Test
    public void withoutTreatmentNorTime() throws Exception {
        Quarantine quarantine = new Quarantine("F,H,D,T");
        assertEquals("F:1 H:1 D:1 T:1 X:0", quarantine.report());
    }

    @Test
    public void withoutTreatment() throws Exception {
        Quarantine quarantine = new Quarantine("F,H,D,D,D,T");
        quarantine.wait40Days();
        // diabetics die without insulin
        assertEquals("F:1 H:1 D:0 T:1 X:3", quarantine.report());
    }

    @Test
    public void aspirin() throws Exception {
        Quarantine quarantine = new Quarantine("F,F,F,H,D,T");
        quarantine.aspirin();
        quarantine.wait40Days();
        // aspirin applyCure Fever
        assertEquals("F:0 H:4 D:0 T:1 X:1", quarantine.report());
    }

    @Test
    public void antibiotic() throws Exception {
        Quarantine quarantine = new Quarantine("F,H,D,D,D,H,T");
        quarantine.antibiotic();
        quarantine.wait40Days();
        // antibiotic applyCure Tuberculosis
        // but healthy people catch Fever if mixed with insulin.
        assertEquals("F:1 H:3 D:0 T:0 X:3", quarantine.report());
    }

    @Test
    public void insulin() throws Exception {
        Quarantine quarantine = new Quarantine("F,H,D,D,D,H,T");
        quarantine.insulin();
        quarantine.wait40Days();
        // insulin prevent diabetic subject from dying, does not applyCure Diabetes,
        assertEquals("F:1 H:2 D:3 T:1 X:0", quarantine.report());
    }

    @Test
    public void antibioticPlusInsulin() throws Exception {
        Quarantine quarantine = new Quarantine("F,H,D,D,D,H,T");
        quarantine.antibiotic();
        quarantine.insulin();
        quarantine.wait40Days();
        // if insulin is mixed with antibiotic, healthy people catch Fever
        assertEquals("F:3 H:1 D:3 T:0 X:0", quarantine.report());
    }

    @Test
    public void paracetamol() throws Exception {
        Quarantine quarantine = new Quarantine("F,F,H,D,D,D,H,T");
        quarantine.paracetamol();
        quarantine.wait40Days();
        // paracetamol heals fever
        assertEquals("F:0 H:4 D:0 T:1 X:3", quarantine.report());
    }

    @Test
    public void paracetamolAndAspirin() throws Exception {
        Quarantine quarantine = new Quarantine("F,H,D,D,D,H,T");
        quarantine.paracetamol();
        quarantine.aspirin();
        // mixing paracetamol & aspirin INSTANTLY kills subject !
        assertEquals("F:0 H:0 D:0 T:0 X:7", quarantine.report());
    }


    @Test
    @Ignore
    public void test() throws Exception {
        int java7 = 0;
        int java8 = 0;
        int parallelJava8 = 0;
        long timeMillis = System.currentTimeMillis();
        List<String> s1 = new ArrayList<String>();
        for (int j = 0; j < 10; j++) {
            System.out.println("Tentative :" + j);
            for (int index = 0; index < 900000; index++) {
                Random random = new Random();
                s1.add(random.nextInt() + "A" + random.nextDouble() + "B" + random.nextGaussian());
            }

            timeMillis = System.currentTimeMillis() - timeMillis;
            System.out.println("time to insert: " + timeMillis);
            long timeMillis1 = System.currentTimeMillis();
        /*Collections.sort(s1);*/
        /*s1.sort((String ss1, String ss2)-> ss1.compareTo(ss2) );*/
            List<String> collect3 = new ArrayList<>();
            for (String currentElement : s1) {
                if (currentElement.contains("5") && currentElement.length() > 5) {
                    collect3.add(currentElement);
                }
            }
            timeMillis1 = System.currentTimeMillis() - timeMillis1;
            System.out.println("time to filter simple way: " + timeMillis1 + ", with size :" + collect3.size());
            long timeMillis2 = System.currentTimeMillis();
        /*Collections.sort(s1);*/
		/*s1.sort((String ss1, String ss2)-> ss1.compareTo(ss2) );*/
            List<String> collect1 = s1.stream().filter(a -> a.contains("5") && a.length() > 5).collect(Collectors.toList());
            timeMillis2 = System.currentTimeMillis() - timeMillis2;
            System.out.println("time to filter by java 8: " + timeMillis2 + ", with size :" + collect1.size());
            long timeMillis3 = System.currentTimeMillis();
		/*Collections.sort(s1);*/
		/*s1.sort((String ss1, String ss2)-> ss1.compareTo(ss2) );*/
            List<String> collect2 = s1.parallelStream().filter(a -> a.contains("5") && a.length() > 5).collect(Collectors.toList());
            timeMillis3 = System.currentTimeMillis() - timeMillis3;
            System.out.println("time to filter by parallel java 8: " + timeMillis3 + ", with size :" + collect2.size());
            long best = Math.max(Math.max(timeMillis1, timeMillis2), timeMillis3);
            String bestOne = ((timeMillis1 == best ? "simple way java 7" : (timeMillis2 == best ? "java 8" : (timeMillis3 == best ? "parallel java 8" : ""))));

            if (bestOne.equals("simple way java 7")) java7++;
            if (bestOne.equals("java 8")) java8++;
            if (bestOne.equals("parallel java 8")) parallelJava8++;
            System.out.println("The best one is --------------------------------------------------> " + bestOne);
        }
        if (java7 > java8 && java7 > parallelJava8) System.out.println(" The best is JAVA7");
        if (java8 > java7 && java8 > parallelJava8) System.out.println(" The best is JAVA8");
        if (parallelJava8 > java8 && parallelJava8 > java7) System.out.println(" The best is JAVA8 parallel");

    }


}